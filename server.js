// server.js
const https = require('https');
const fs = require('fs');
const express = require('express');
const path = require('path');

const options = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};

var app = express();

app.use(express.static(path.join(__dirname + '/build')));

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname + '/build', "index.html"))
})

https.createServer(options, app).listen(3005);