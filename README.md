
## Service Worker

### Getting started

Install
```
npm install
```

Start server
```
node serve.js
```

Go to URL
```
https://localhost:3005
```


### HTTPS Localhost
create cert file :

install mkcert

```
brew install mkcert
```

generate key & cert pem file

```
mkcert -key-file key.pem -cert-file cert.pem localhost
```
This command will generate two pem files.

- key.pem
- cert.pem


### View running service worker

```
chrome://serviceworker-internals/
```
