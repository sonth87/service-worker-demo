console.log("----- start service worker -----")

if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("service_worker.js")
    .then((reg) => {
      console.log("Registered service worker");
    })
    .catch((err) => {
      console.log("Register service worker failed ------ ", err);
    });
}